import os

import pytest

from almalinux.gitutils.common import *


@pytest.mark.parametrize(
    'checksum,expected',
    [('35d14f5ab4ee239b070f3b645fb82837', 'md5'),
     ('1014c8812720619a5a6bcd189e5d7f5d16276d86', 'sha1'),
     ('86d8a9a32cdaff2c6003c67a12549466319e0ae51b7665fd01fd9354a3b1cf55',
      'sha256'),
     ('9906e61ef0b693bf978e2a88b737c79dd2c815cfc1a09443f04b79b994b4646ff72f18'
      '6e42461b3a5768667119f39fa006ce71530791a5b35c2278e9252ec3ea', 'sha512')]
)
def test_detect_checksum_type(checksum, expected):
    """detect_checksum_type returns type for supported checksums"""
    assert detect_checksum_type(checksum) == expected


def test_detect_checksum_type_error():
    """detect_checksum_type raises ValueError if checksum type is unknown"""
    with pytest.raises(ValueError):
        detect_checksum_type('somethingwrong')


@pytest.mark.parametrize(
    'checksum_type,checksum',
    [(None, '06364afe79d801433188262478a76d19777ef351'),
     ('sha1', '06364afe79d801433188262478a76d19777ef351'),
     ('sha256', 'b37758528c0338d529b3fb16fd39f28da58241abc856e16bf0bc8b99c60cd632')]
)
def test_get_file_checksum(tmpdir, checksum_type, checksum):
    """get_file_checksum supports different checksum types"""
    file_path = os.path.join(tmpdir, 'test_file.txt')
    with open(file_path, 'w') as fd:
        fd.write('TESTDATA\n')
    args = [file_path]
    if checksum_type:
        args.append(checksum_type)
    assert get_file_checksum(*args) == checksum


def test_find_metadata_file_single(tmpdir):
    """find_metadata_file returns a single metadata file"""
    file_path = os.path.join(tmpdir, '.project.metadata')
    open(file_path, 'a').close()
    assert find_metadata_file(tmpdir) == file_path


def test_find_metadata_file_missing(tmpdir):
    """find_metadata_file raises Exception when metadata file is not found"""
    with pytest.raises(Exception):
        find_metadata_file(tmpdir)


def test_find_metadata_file_multiple(tmpdir):
    """
    find_metadata_file raises Exception when there are multiple metadata files
    """
    for i in range(2):
        open(os.path.join(tmpdir, f'.project{i}.metadata'), 'a').close()
    with pytest.raises(Exception):
        find_metadata_file(tmpdir)


def test_iter_metadata(tmpdir):
    """iter_metadata returns checksums from metadata file"""
    data = [
        ('SOURCES/mc-4.8.19.tar.xz',
         '850747ae43a5c81f1dd0d906dfa9e149eb19748a', 'sha1'),
        ('SOURCES/binary-blob',
         'b37758528c0338d529b3fb16fd39f28da58241abc856e16bf0bc8b99c60cd632',
         'sha256')
    ]
    metadata_path = os.path.join(tmpdir, '.project.metadata')
    with open(metadata_path, 'w') as fd:
        for rec in data:
            fd.write(f'{rec[1]} {rec[0]}\n')
    metadata = []
    for file_path, checksum, checksum_type in iter_metadata(metadata_path):
        metadata.append((file_path, checksum, checksum_type))
    assert metadata == data


def test_normalize_path(monkeypatch):
    """
    normalize_path expands variables and converts relative paths to absolute
    """
    cwd = os.getcwd()
    expected = os.path.join(cwd, 'basedir', 'subdir')
    monkeypatch.setenv('BASE_DIR', 'basedir')
    assert normalize_path('${BASE_DIR}/subdir') == expected
